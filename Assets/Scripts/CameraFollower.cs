﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class CameraFollower : MonoBehaviour
{
    [SerializeField] private Transform target;
    private Vector3 offset;
    private void OnEnable()
    {
        //GameManager.OnJump += CameraMove;
        PlayerBehaviour.OnDeath += StopCamera;
    }

    private void OnDisable()
    {
        //GameManager.OnJump -= CameraMove;
        PlayerBehaviour.OnDeath -= StopCamera;
    }

    private void Awake()
    {
        offset = target.position - transform.position;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 position = transform.position;
        position = target.position - offset;
        transform.position = Vector3.Lerp(transform.position, position, Time.deltaTime);
    }

    public void StopCamera()
    {
        this.enabled = false;
    }

    public void CameraMove()
    {
        gameObject.transform.DOMove(new Vector3(1, 0, 0), 0.3f).SetRelative();
    }
}

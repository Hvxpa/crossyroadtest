﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using DigitalRuby.Pooling;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
    public float playerJumpTime;
    public float playerJumpHeight;
    public TMP_Text scoreText;
    
    public static int coinNumber;
    public static int scoreRecord;
    public static bool isSoundOn;
    public static bool canStart;
    private Transform player;
    private float lastJumpTime = 0f;
    private int score;
    private Vector3 firstTouch;
    private Vector3 lastTouch;
    private Vector3 target;
    private float dragDistance;
    private Camera camera;
    private bool isRunStarted;
    private bool isPlayerAlive;
    

    public static event Action OnJump;
    public static event Action OnGameStart;
    public static event Action OnRestart;
    public static event Action OnRunStart;

    private void OnEnable()
    {
       
        OnRestart += Restart;
        PlayerBehaviour.OnDeath += PlayerDeath;
        score = 0;
    }

    private void OnDisable()
    {
        
        OnRestart -= Restart;
        PlayerBehaviour.OnDeath -= PlayerDeath;
    }

    // Start is called before the first frame update
    void Start()
    {
        
        player = GameObject.Find("Player").GetComponent<Transform>();
        isRunStarted = false;
        isSoundOn = true;
        canStart = false;
        dragDistance = Screen.height * 0.05f;
        camera = Camera.main;
        isPlayerAlive = true;
        PlayerData data = SaveSystem.LoadData();
        if (data != null)
        {
            coinNumber = data.coins;
            scoreRecord = data.record;
            isSoundOn = data.isSoundOn;
            
            if (!isSoundOn)
            {
                Camera.main.GetComponent<AudioListener>().enabled = false;
            }
            else
            {
                Camera.main.GetComponent<AudioListener>().enabled = true;
            }
        }
        OnGameStart();
    }

    // Update is called once per frame
    void Update()
    {
        
        if (Application.platform == RuntimePlatform.Android)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (!isPlayerAlive) OnRestart();
                if (isPlayerAlive && !isRunStarted) Application.Quit();
            }
        }

        if (score > scoreRecord)
        {
            scoreRecord = score;
        }

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            OnRestart();
        }
        if (canStart)
        {
            foreach (Touch touch in Input.touches)
            {
                if (touch.phase == TouchPhase.Began)
                {
                    firstTouch = touch.position;
                    lastTouch = touch.position;

                    if (!isRunStarted)
                    {
                        isRunStarted = true;
                        OnRunStart();
                    }
                    Sequence sequence = DOTween.Sequence();
                    sequence.Append(player.DOScaleY(-0.2f, 0.1f).SetRelative().SetEase(Ease.Linear))
                        .Join(player.DOScaleZ(0.2f, 0.1f).SetRelative().SetEase(Ease.Linear));
                }
                if (touch.phase == TouchPhase.Ended)
                {

                    lastTouch = touch.position;
                    target = lastTouch - firstTouch;
                    player.DOScale(new Vector3(1.7f, 1.7f, 1.7f), 0.1f).SetEase(Ease.Linear);
                    if ((Time.time - lastJumpTime > playerJumpTime) && isPlayerAlive)
                    {
                        if (Mathf.Abs(lastTouch.x - firstTouch.x) > dragDistance || Mathf.Abs(lastTouch.y - firstTouch.y) > dragDistance)
                        {
                            if (Mathf.Abs(lastTouch.x - firstTouch.x) > Mathf.Abs(lastTouch.y - firstTouch.y))
                            {
                                if (lastTouch.x > firstTouch.x)
                                {
                                    Move(MoveDirection.Left);
                                }
                                else
                                {
                                    Move(MoveDirection.Right);
                                }
                            }
                            else
                            {
                                if (lastTouch.y > firstTouch.y)
                                {

                                    Move(MoveDirection.Forward);
                                }
                                else
                                {
                                    Move(MoveDirection.Back);
                                }
                            }
                        }
                        else
                        {
                            Move(MoveDirection.Forward);
                        }
                        lastJumpTime = Time.time;
                    }

                }
            }
        }
    }

    public void Jump()
    {
        player.parent = null;
       
        float desiredPosition = Mathf.Round(player.position.z);
        player.rotation = Quaternion.Euler(Vector3.zero);
        player.DOLocalJump(new Vector3(1, 0, desiredPosition - player.position.z), playerJumpHeight, 1, playerJumpTime)
            .SetEase(Ease.Linear)
            .SetRelative()
            .OnComplete(IncreaseScore);
    }

    public void Move(MoveDirection direction)
    {
        float desiredPosition = Mathf.Round(player.position.z);
        switch (direction)
        {
            case MoveDirection.Forward:
                player.parent = null;

                
                player.rotation = Quaternion.Euler(Vector3.zero);
                player.DOLocalJump(new Vector3(1, 0, desiredPosition - player.position.z), playerJumpHeight, 1, playerJumpTime)
                    .SetEase(Ease.Linear)
                    .SetRelative()
                    .OnComplete(IncreaseScore);
                break;
            case MoveDirection.Back:
                player.parent = null;
                player.rotation = Quaternion.Euler(0, 180, 0);
                player.DOLocalJump(new Vector3(-1, 0, desiredPosition - player.position.z), playerJumpHeight, 1, playerJumpTime)
                    .SetEase(Ease.Linear)
                    .SetRelative();
                break;
            case MoveDirection.Left:
                player.DOLocalJump(new Vector3(0, 0, -1f), playerJumpHeight, 1, playerJumpTime).SetRelative().SetEase(Ease.Linear);
                player.rotation = Quaternion.Euler(0, 90, 0);
                break;
            case MoveDirection.Right:
                player.DOLocalJump(new Vector3(0, 0, 1f), playerJumpHeight, 1, playerJumpTime).SetRelative().SetEase(Ease.Linear);
                player.rotation = Quaternion.Euler(0, -90, 0);
                break;
        }
        OnJump();
    }

    public void IncreaseScore()
    {
        score++;
        scoreText.text = score.ToString();
    }

    public void Restart()
    {
        DOTween.KillAll();
        SpawningPool.RecycleActiveObjects();
        SaveSystem.SaveData();
        isRunStarted = false;
        
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    private void OnApplicationQuit()
    {
        SaveSystem.SaveData();
    }

    public void PlayerDeath()
    {
        isPlayerAlive = false;
    }

    public void RestartButtonClicked()
    {
        OnRestart();
    }
}

public enum MoveDirection
{
    Forward,
    Back,
    Left,
    Right
}

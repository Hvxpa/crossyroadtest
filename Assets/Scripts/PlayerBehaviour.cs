﻿using System;
using System.Collections;
using System.Collections.Generic;
using DigitalRuby.Pooling;
using DG.Tweening;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{
    public AudioSource jumpSound;
    public AudioSource coinPickupSound;
    public AudioSource deathSound;
    public static event Action OnDeath;

    private void OnEnable()
    {
        OnDeath += Death;
        GameManager.OnJump += JumpSound;
    }

    private void OnDisable()
    {
        OnDeath -= Death;
        GameManager.OnJump -= JumpSound;
    }




    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Log")
        {
            transform.parent = other.transform;
            Debug.Log("WOOWWOW");

            float desiredPosition;
            desiredPosition = Mathf.Clamp(Mathf.Round(transform.localPosition.z) + 0.5f * ((other.transform.localScale.z + 1) % 2), -0.5f * (other.transform.localScale.z - 1), 0.5f * (other.transform.localScale.z - 1));

            transform.DOLocalMoveZ(desiredPosition, 0.1f).SetEase(Ease.Linear);


        }

        if (other.transform.tag == "Coin")
        {
            coinPickupSound.Play();
            GameManager.coinNumber++;
            SpawningPool.ReturnToCache(other.gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Obstacle")
        {
            OnDeath();
        }
    }

    public void Death()
    {
        deathSound.Play();
        this.GetComponent<Collider>().enabled = false;
        this.enabled = false;
    }

    public void JumpSound()
    {
        jumpSound.Play();
    }
}

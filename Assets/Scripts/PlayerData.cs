﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData
{
    public int record;
    public int coins;
    public bool isSoundOn;

    public PlayerData()
    {
        record = GameManager.scoreRecord;
        coins = GameManager.coinNumber;
        isSoundOn = GameManager.isSoundOn;
    }
}

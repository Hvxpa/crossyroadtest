﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using DigitalRuby.Pooling;
using UnityEngine;

public class Road : MonoBehaviour
{
    public float minDuration;
    public float maxDuration;
    public float minDelay;
    public float maxDelay;
    public ObjectDirection carDirection;
    public int carIndex;
    private float _duration;
    private float _currentDelay;
    private float _lastTimeSpawned;
    // Start is called before the first frame update
    void Start()
    {
        _duration = Random.Range(minDuration, maxDuration);
        if (Random.Range(0, 2) == 0)
        {
            carDirection = ObjectDirection.Left;
        }
        else
        {
            carDirection = ObjectDirection.Right;
        }

        _currentDelay = Random.Range(minDelay, maxDelay);
        _lastTimeSpawned = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time - _lastTimeSpawned >= _currentDelay)
        {
            SpawnCar();
            _lastTimeSpawned = Time.time;
            _currentDelay = Random.Range(minDelay, maxDelay);
        }
    }

    private void SpawnCar()
    {
        Vector3 position;
        Vector3 endPosition;
        Quaternion rotation = Quaternion.identity;
        if (carDirection == ObjectDirection.Left)
        {
            position = new Vector3(transform.position.x, 0.4f, -10);
            endPosition = new Vector3(transform.position.x, 0.4f, 10);
        }
        else
        {
            position = new Vector3(transform.position.x, 0.4f, 10);
            endPosition = new Vector3(transform.position.x, 0.4f, -10);
            rotation = Quaternion.Euler(0, 180, 0);
        }
        GameObject car = SpawningPool.CreateFromCache("Car" + carIndex.ToString());
        car.transform.position = position;
        car.transform.rotation = rotation;
        car.transform.DOMove(endPosition, _duration).SetEase(Ease.Linear).OnComplete(() => DestroyCar(car));
    }

    public void DestroyCar(GameObject car)
    {
        SpawningPool.ReturnToCache(car);
    }
}

public enum ObjectDirection
{
    Left,
    Right
}

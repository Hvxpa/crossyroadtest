﻿using System.Collections;
using System.Collections.Generic;
using DigitalRuby.Pooling;
using UnityEngine;

public class TerrainGenerator : MonoBehaviour
{
    public List<TerrainObject> terrainObjects = new List<TerrainObject>();
    private int x = -3;
    private int currentObjectIndex = 0;
    private int objectNumberRemain = 5;
    private ObjectDirection directionForWater = ObjectDirection.Left;

    private void OnEnable()
    {
        GameManager.OnJump += CreateTerrainObject;
        GameManager.OnGameStart += StartGeneration;
        
    }

    private void OnDisable()
    {
        GameManager.OnJump -= CreateTerrainObject;
        GameManager.OnGameStart -= StartGeneration;
        
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (objectNumberRemain == 0)
        {
            NewObjectType();
        }
    }

    public void CreateTerrainObject()
    {
        TerrainObject terrainObject = terrainObjects[currentObjectIndex];
        var newTerrain = SpawningPool.CreateFromCache(terrainObject.name);
        if(terrainObjects[currentObjectIndex].terrainType == TerrainType.Road)
        {
            newTerrain.GetComponent<Road>().carIndex = Random.Range(0, 3);
        }
        if (!(terrainObjects[currentObjectIndex].terrainType == TerrainType.Water))
        {
            if (Random.value < 0.05f)
            {
                var newCoin = SpawningPool.CreateFromCache("Coin");
                newCoin.transform.position = new Vector3(x, 0.5f, Random.Range(-4, 5));
                newCoin.SetActive(true);
            }
        }
        else
        {
            newTerrain.GetComponent<Water>().logDirection = directionForWater;
            if (directionForWater == ObjectDirection.Left)
            {
                directionForWater = ObjectDirection.Right;
            }
            else
            {
                directionForWater = ObjectDirection.Left;
            }
        }
        newTerrain.transform.position = new Vector3(x, 0, 0);
        newTerrain.SetActive(true);
        objectNumberRemain--;
        x++;
    }

    public void NewObjectType()
    {


        switch (currentObjectIndex)
        {
            case 0:
                if (Random.value <= 0.25f)
                {
                    currentObjectIndex = 2;
                }
                else
                {
                    currentObjectIndex = 1;
                }
                break;
            case 1:
                if (Random.value <= 0.25f)
                {
                    currentObjectIndex = 2;
                }
                else
                {
                    currentObjectIndex = 0;
                }
                break;
            case 2:
                if (Random.value <= 0.5f)
                {
                    currentObjectIndex = 0;
                }
                else
                {
                    currentObjectIndex = 1;
                }
                break;

        }

        objectNumberRemain = Random.Range(terrainObjects[currentObjectIndex].minimumElementNumber, terrainObjects[currentObjectIndex].maximumElementNumber + 1);
        
    }

    void StartGeneration()
    {
        for (int i = 0; i < 8; i++)
        {
            CreateTerrainObject();
        }
        NewObjectType();
        for (int i = 0; i < 20; i++)
        {
            CreateTerrainObject();
            if (objectNumberRemain == 0)
            {
                NewObjectType();
            }
        }
    }

   
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class TerrainObject : ScriptableObject
{
    public string name;
    public TerrainType terrainType;
    public GameObject terrainPrefab;
    public int minimumElementNumber;
    public int maximumElementNumber;
}

public enum TerrainType
{
    Grass,
    Road,
    Water
}

﻿using System.Collections;
using System.Collections.Generic;
using DigitalRuby.Pooling;
using UnityEngine;

public class Trn : MonoBehaviour
{
    public TerrainType terrainType;
    private Transform player;

    private void Start()
    {
        
    }

    private void OnEnable()
    {
        player = GameObject.Find("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x - player.position.x <= -10f)
        {
            SpawningPool.ReturnToCache(gameObject);
        }
    }
}

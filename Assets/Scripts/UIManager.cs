﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public Image panel;
    public RawImage logo;
    public RawImage pointer;
    public Texture pointer0;
    public Texture pointer1;
    public TMP_Text coinText;
    public TMP_Text scoreText;
    public TMP_Text recordText;
    public GameObject loseScreen;
    public Button soundButton;
    public Sprite soundOnSprite;
    public Sprite soundOffSprite;

    private void OnEnable()
    {
        GameManager.OnGameStart += ShowLogo;
        GameManager.OnRunStart += HideElements;
        PlayerBehaviour.OnDeath += DeathScreen;
    }

    private void OnDisable()
    {
        GameManager.OnGameStart -= ShowLogo;
        GameManager.OnRunStart -= HideElements;
        PlayerBehaviour.OnDeath -= DeathScreen;
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        coinText.text = GameManager.coinNumber.ToString();
    }

    public void ShowLogo()
    {
        panel.gameObject.SetActive(true);
        logo.gameObject.SetActive(true);
        pointer.gameObject.SetActive(true);
        StartCoroutine(Pointing());
        StartCoroutine(WaitAndHidePanel());
    }

    public void HideElements()
    {
        if (GameManager.isSoundOn)
        {
            soundButton.image.sprite = soundOnSprite;
        }
        else
        {
            soundButton.image.sprite = soundOffSprite;
        }
        logo.rectTransform.DOAnchorPosX(1050f, 0.5f);
        pointer.gameObject.SetActive(false);
        scoreText.gameObject.SetActive(true);
    }

    public void DeathScreen()
    {
        loseScreen.SetActive(true);
        recordText.text = "record " + GameManager.scoreRecord;
    }

    public void ToggleSound()
    {
        GameManager.isSoundOn = !GameManager.isSoundOn;
        if (GameManager.isSoundOn)
        {
            soundButton.image.sprite = soundOnSprite;
            Camera.main.GetComponent<AudioListener>().enabled = true;
        }
        else
        {
            soundButton.image.sprite = soundOffSprite;
            Camera.main.GetComponent<AudioListener>().enabled = false;
        }
    }

    IEnumerator WaitAndHidePanel()
    {
        yield return new WaitForSeconds(3.0f);
        panel.DOFade(0, 0.5f);
        GameManager.canStart = true;
    }

    IEnumerator Pointing()
    {
        while(pointer.isActiveAndEnabled)
        {
            pointer.rectTransform.anchoredPosition = new Vector2(0, -530);
            pointer.texture = pointer0;
            yield return new WaitForSeconds(0.3f);
            pointer.rectTransform.anchoredPosition = new Vector2(0, -520);
            pointer.texture = pointer1;
            yield return new WaitForSeconds(0.3f);
        }
    }

}

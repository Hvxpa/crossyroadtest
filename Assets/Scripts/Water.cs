﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using DigitalRuby.Pooling;
using UnityEngine;

public class Water : MonoBehaviour
{
    public float minDuration;
    public float maxDuration;
    public float minDelay;
    public float maxDelay;
    public ObjectDirection logDirection;
    private float _duration;
    private float _currentDelay;
    private float _lastTimeSpawned;
    // Start is called before the first frame update
    void Start()
    {
        _duration = Random.Range(minDuration, maxDuration);
        

        _currentDelay = Random.Range(minDelay, maxDelay);
        _lastTimeSpawned = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time - _lastTimeSpawned >= _currentDelay)
        {
            SpawnLog();
            _lastTimeSpawned = Time.time;
            _currentDelay = Random.Range(minDelay, maxDelay);
        }
    }

    public void SpawnLog()
    {
        Vector3 position;
        Vector3 endPosition;
        if (logDirection == ObjectDirection.Left)
        {
            position = new Vector3(transform.position.x, 0, -10);
            endPosition = new Vector3(transform.position.x, 0, 10);
        }
        else
        {
            position = new Vector3(transform.position.x, 0, 10);
            endPosition = new Vector3(transform.position.x, 0, -10);
        }

        GameObject log = SpawningPool.CreateFromCache("Log" + Random.Range(0, 3).ToString());
        log.transform.position = position;
        log.transform.DOMove(endPosition, _duration).SetEase(Ease.Linear).OnComplete(() => DestroyLog(log));
    }

    public void DestroyLog(GameObject log)
    {
        SpawningPool.ReturnToCache(log);
    }
}
